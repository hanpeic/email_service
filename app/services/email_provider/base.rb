class EmailProvider::Base
  def initialize(settings:)
    @server = settings[:server]
    @key = settings[:key]
  end

  def send(email)
    raise NotImplementedError, "send must be implemented in child class"
  end
end
