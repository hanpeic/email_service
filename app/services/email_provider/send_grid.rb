class EmailProvider::SendGrid < EmailProvider::Base
  def send(email)
    toes = []
    email.to_list.each do |to|
      toes << { "email" => to }
    end
    ccs = []
    email.cc_list.each do |cc|
      ccs << { "email" => cc }
    end
    bccs = []
    email.bcc_list.each do |bcc|
      bccs << { "email" => bcc }
    end
    data = {
      "personalizations" => [{
        "to" => toes,
        "cc" => ccs,
        "bcc" => bccs
      }],
      "from" => from,
      "subject" => emai.subject,
      "content" => [{"type" => "text/plain", "value" => email.body }]
    }

    url = @server
    headers = { 'Content-Type' => 'application/json', 'Authorization' => "Bearer #{@key}" } 
    response = RestClient.post url, data.to_json, headers
    return ture if response.code >= 200 && response.code < 300
  rescue StandardError => e
    nil
  end
end
