class EmailProvider::MailGun < EmailProvider::Base
  def send(email)
    data = { 'from' => email.from,
      'to' => email.to,
      'subject' => email.subject,
      'text' => email.body,
      'cc' => email.cc,
      'bcc' => email.bcc
    }
    url = "https://api:key-#{@key}@#{@server}"
    response = RestClient.post url, data
    return ture if response.code >= 200 && response.code < 300
  rescue StandardError => e
    nil
  end
end
