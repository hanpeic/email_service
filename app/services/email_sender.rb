class EmailSender
  attr_accessor :email, :send_service

  def initialize(email)
    @email = email
    @send_services = [EmailProvider::SendGrid.new(server: ENV['SENDGRID_SERVER'], key: ENV['SENDGRID_KEY']),
      EmailProvider::MailGun.new(server: ENV['MAILGUN_SERVER'], key: ENV['MAILGUN_KEY'])]
  end

  def perform
    success = false
    @send_services.each do |send_service|
      if send_service.send(email)
        success = true
        break
      end
    end
    success
  end
end
