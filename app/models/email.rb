class Email < ApplicationRecord
  VALID_EMAIL = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  validates :from, presence: true, format: { with: VALID_EMAIL }
  validates :to, presence: true
  validates :subject, presence: true, length: { in: 1..80 }
  validates :body, presence: true
  validate :emails_valid

  def to_list
    return to.split(';')  if to.present?
    []
  end

  def cc_list
    return cc.split(';')  if cc.present?
    []
  end

  def bcc_list
    return bcc.split(';')  if bcc.present?
    []
  end

  private
  def emails_valid
    emails = to_list + cc_list + bcc_list
    emails.each do |email|
      unless is_a_valid_email?(email)
        errors.add(:base, "must be valid email")
        break
      end
    end
  end

  def is_a_valid_email?(email)
    (email =~ VALID_EMAIL)
  end
end
