class Api::V1::EmailsController < ActionController::Base
  def create
    @email = Email.new(email_params)

    if @email.valid? && EmailSender.new(@email).perform
        @email.save
        render json: { message: 'success' }, status: 200
    else
      render json: { message: 'send failed' }, status: 422
    end
  end

  def email_params
    params.require(:email)
  end
end
