# Email Service
A service that accepts the necessary information and sends emails.

## Installation

1 Install ruby

2 Download the code: git clone https://tom_han@bitbucket.org/tom_han/email_service.git

3 Run command: bundle install

4 Start the server: rails s

## Usage

Execute the following code in your command line:

```
curl --request POST \
  --url http://localhost:3000/api/v1/mails \
  --data '{"email": {"from": "test@example.com", "to": "test@example.com", "subject": "sending test email", "body": "This is email body."}}'
```
