class CreateEmails < ActiveRecord::Migration[5.1]
  def change
    create_table :emails do |t|
      t.string :from
      t.string :to
      t.string :subject
      t.text :body
      t.string :cc
      t.string :bcc

      t.timestamps
    end
  end
end
